(function () {
    'use strict';

    angular.module('app.products').controller('ProductsController', ProductsController);

    ProductsController.$inject = ['$scope', '$http', '$state', '$stateParams', 'productsManager', '$ionicSearchPanel', '$ionicScrollDelegate', '$rootScope'];
    
    
    function ProductsController($scope, $http, $state, $stateParams, productsManager, $ionicSearchPanel, $ionicScrollDelegate, $rootScope) {
        
        var vm = this,
            categoryId = $stateParams.categoryId,
            searchKey = $stateParams.searchKey,
            viewType = $stateParams.viewType,
            currentPage = 0,
            PAGE_SIZE = 30,
            requestTimeout;
            
        // properties
        vm.products = [];
        vm.subcategories = productsManager.getNavigationLinks(categoryId);
        vm.viewOptions = productsManager.viewOptions;
        vm.showTabs = false;
        vm.activeSlide = 0;
        vm.searchKey = searchKey;
        vm.productsLoading = false;
        vm.infiniteLoading = false;
        vm.noMoreProducts = false;
        vm.productRequest = null;
        // set view type specific properties
        productsManager.initViewModel(vm, viewType);
        
        // methods
        vm.handleSlideChange = handleSlideChange;
        vm.toggleViewMode = toggleViewMode;
        vm.loadMoreProducts = loadMoreProducts;
        vm.navigateToProductDetails = navigateToProductDetails;
        vm.handleSortChange = handleSortChange;
        
        $scope.$on('$ionicView.afterEnter', function (event, transition) {
            if (transition.direction !== 'back') {
                if (vm.hasTabs) {            
                    vm.showTabs = true;          
                    handleSlideChange(0);
                } else {
                    vm.productRequest = loadProducts(categoryId, searchKey).then(function (products) {
                        $ionicScrollDelegate.scrollTop();
                        return applyProducts(products);

                    });
                }
            }
        });
        
        $scope.$on('search.submit', function (event, key) {
            searchKey = key;
            vm.pageTitle = searchKey;
            resetState();
            vm.productRequest = loadProducts(categoryId, searchKey).then(function (products) {
                return applyProducts(products);
            });
        });
        
        function handleSlideChange(index) {
            if (vm.subcategories && index in vm.subcategories) {
                categoryId = vm.subcategories[index].id;
                resetState();
                vm.productRequest = loadProducts(categoryId, '').then(function (products) {
                    return applyProducts(products);
                });
            }
        }
        
        function loadProducts(categoryId, searchKey) { 
            
            var requestPromise = productsManager.findProducts({
                key: searchKey,
                category: categoryId,
                page: currentPage,
                pageSize: PAGE_SIZE,
                sort: 'date_asc'
            });

            requestTimeout = requestPromise.httpTimeout;

            vm.productsLoading = true;            

            return requestPromise.then(function (result) {
                vm.productsLoading = false;            
                return result;
            }, function (err) {
                if (err.status !== -1) {
                    vm.productsLoading = false;                    
                }
                throw err;
            });
        }
      /*
       * Change here
       */
        function loadMoreProducts() {
            currentPage++;
            vm.infiniteLoading = true;
            
            loadProducts(categoryId, searchKey).then(function (products) {
                if (products.length) {
                    //addition test of changing price in array of objects sent to page
                    products.forEach(function(product){
                      if(($rootScope.globalCurrencyChangeRate !== "") && ($rootScope.globalCurrencyChangeRate !== undefined)){
                        product._price = parseFloat(product._price).toFixed(2) * parseFloat($rootScope.globalCurrencyChangeRate).toFixed(2);
                        
                        if(product._salePrice !== null){                        
                          product._salePrice = parseFloat(product._salePrice).toFixed(2) * parseFloat($rootScope.globalCurrencyChangeRate).toFixed(2); 
                        }
                      }
                      if(product._variants.length > 0){
                        
                        product._variants.forEach(function(variant){
                          //changing price here if needed
                          if(($rootScope.globalCurrencyChangeRate !== "") && ($rootScope.globalCurrencyChangeRate !== undefined)){
                            variant._price = variant._price * $rootScope.globalCurrencyChangeRate;
                            if(variant._salePrice !== null){                        
                              variant._salePrice = parseFloat(variant._salePrice).toFixed(2) * parseFloat($rootScope.globalCurrencyChangeRate).toFixed(2); 
                            }
                          }
                          
                        });
                      }
                    }); 
                    vm.products = vm.products.concat(products);

                    if (products.length < PAGE_SIZE) {
                        vm.noMoreProducts = true;
                    }
                } else {
                    vm.noMoreProducts = true;
                }

            }).finally(function () {
                $scope.$broadcast('scroll.infiniteScrollComplete');
                vm.infiniteLoading = false;
            });
        }
        
        function navigateToProductDetails(product) {
            productsManager.setCurrentProduct(product);
            $state.go('app.product-details', {
                'productId': product.id
            });
        }

        function toggleViewMode() {
            productsManager.toggleViewMode();
        }
        
        function handleSortChange() {
            resetState();            
            vm.productRequest = loadProducts(categoryId, searchKey).then(function (products) {
                return applyProducts(products);
            });
        }

        function resetState() {
            currentPage = 0;
            vm.noMoreProducts = false;
            if (requestTimeout) {
                requestTimeout.resolve();
                requestTimeout = null;                
            }           
        }
        /*
       * Change here
       */
        function applyProducts(products) {
          
            products.forEach(function(product){
         
              if(($rootScope.globalCurrencyChangeRate !== "") && ($rootScope.globalCurrencyChangeRate !== undefined)){
                product._price = parseFloat(product._price).toFixed(2) * parseFloat($rootScope.globalCurrencyChangeRate).toFixed(2);      
                
                if(product._salePrice !== null){                        
                  product._salePrice = parseFloat(product._salePrice).toFixed(2) * parseFloat($rootScope.globalCurrencyChangeRate).toFixed(2); 
                }
              }              
              
              if(product._variants.length > 0){
                
                product._variants.forEach(function(variant){
                    //changing price here if needed
                    if(($rootScope.globalCurrencyChangeRate !== "") && ($rootScope.globalCurrencyChangeRate !== undefined)){
                      
                      variant._price = parseFloat(variant._price).toFixed(2) * parseFloat($rootScope.globalCurrencyChangeRate).toFixed(2);
                      
                      if(variant._salePrice !== null){                        
                        variant._salePrice = parseFloat(variant._salePrice).toFixed(2) * parseFloat($rootScope.globalCurrencyChangeRate).toFixed(2); 
                      }
                    }
                  });
                }
              });
            
            if( ($rootScope.globalCountry !== '') && ($rootScope.globalCountry !== undefined)){
              vm.globalCountry = $rootScope.globalCountry;  
            }
            vm.products = products; 
            return products;
        }
    }

})();